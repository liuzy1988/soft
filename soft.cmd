@echo off
cd "D:\_NodeJS\soft"
echo 1. C32Asm.zip 	 1.31 MB
echo 2. CPUZ.zip 	 1.48 MB
echo 3. DiskGenius.zip 	 19.48 MB
echo 4. DiskInfo.zip 	 3.9 MB
echo 5. ExeinfoPe.zip 	 1.69 MB
echo 6. FastCopy.zip 	 758.13 KB
echo 7. freeSSHd.zip 	 722.8 KB
echo 8. GPUZ.zip 	 13.16 MB
echo 9. HexCmp.zip 	 488.22 KB
echo 10. ILSpy.zip 	 3.12 MB
echo 11. MusicTools.zip 	 4.81 MB
echo 12. Ollydbg.zip 	 20.43 MB
echo 13. ProcView.exe 	 1.12 MB
echo 14. Shadowsocks.zip 	 2.32 MB
echo 15. SPY++.zip 	 551.87 KB
echo 16. TcpView.exe 	 293.78 KB
echo 17. WinNTSetup.zip 	 3.28 MB
echo 18. 一键修改系统信息.exe 	 1.72 MB
echo 19. ant_1.3.4.zip 	 11.55 MB
echo 20. ASPack.zip 	 6.55 MB
echo 21. ConfuserEx.zip 	 2.33 MB
echo 22. ConfuserExUn.zip 	 519.16 KB
echo 23. de4dot.zip 	 1.59 MB
echo 24. dotNET_Reactor.zip 	 24.61 MB
echo 25. DSignTool.zip 	 1.85 MB
echo 26. EasyWeb.zip 	 61.78 KB
echo 27. freeSSHd.exe 	 856.34 KB
echo 28. upx.exe 	 403 KB
echo 29. VMProtect.zip 	 30.95 MB
echo 30. 先驱正式版2020081801.zip 	 26.85 MB
echo 31. 易语言5.9.zip 	 57 MB
echo 32. 神泣0777.zip 	 1.06 MB
echo 33. 神泣GM助手.zip 	 2.25 MB
echo 34. 神泣全能助手.zip 	 1.54 MB
echo 35. 神泣工具全集2011.zip 	 2.77 MB
echo 36. 鼎盛神泣合区工具破解版.zip 	 1.36 MB
echo 37. ATI2021_v25.7.1.39184_x64_WinPE.exe 	 29.1 MB
echo 38. ATI2021_v25.7.1.39184_x86_WinPE.exe 	 26.74 MB
echo 39. DirectX_Repair_V3.9.7z 	 113.41 MB
echo 40. Xshell.zip 	 4.84 MB
:Step0
set choice=''
set /p choice=输入序号: 
if %choice%==1 goto Step1
if %choice%==2 goto Step2
if %choice%==3 goto Step3
if %choice%==4 goto Step4
if %choice%==5 goto Step5
if %choice%==6 goto Step6
if %choice%==7 goto Step7
if %choice%==8 goto Step8
if %choice%==9 goto Step9
if %choice%==10 goto Step10
if %choice%==11 goto Step11
if %choice%==12 goto Step12
if %choice%==13 goto Step13
if %choice%==14 goto Step14
if %choice%==15 goto Step15
if %choice%==16 goto Step16
if %choice%==17 goto Step17
if %choice%==18 goto Step18
if %choice%==19 goto Step19
if %choice%==20 goto Step20
if %choice%==21 goto Step21
if %choice%==22 goto Step22
if %choice%==23 goto Step23
if %choice%==24 goto Step24
if %choice%==25 goto Step25
if %choice%==26 goto Step26
if %choice%==27 goto Step27
if %choice%==28 goto Step28
if %choice%==29 goto Step29
if %choice%==30 goto Step30
if %choice%==31 goto Step31
if %choice%==32 goto Step32
if %choice%==33 goto Step33
if %choice%==34 goto Step34
if %choice%==35 goto Step35
if %choice%==36 goto Step36
if %choice%==37 goto Step37
if %choice%==38 goto Step38
if %choice%==39 goto Step39
if %choice%==40 goto Step40
goto Step0
:Step1
echo 准备下载 C32Asm.zip ...
wget -c -O "D:\soft\C32Asm.zip" "http://upload.liuzy88.com/C32Asm.zip"
goto Step0
:Step2
echo 准备下载 CPUZ.zip ...
wget -c -O "D:\soft\CPUZ.zip" "http://upload.liuzy88.com/CPUZ.zip"
goto Step0
:Step3
echo 准备下载 DiskGenius.zip ...
wget -c -O "D:\soft\DiskGenius.zip" "http://upload.liuzy88.com/DiskGenius.zip"
goto Step0
:Step4
echo 准备下载 DiskInfo.zip ...
wget -c -O "D:\soft\DiskInfo.zip" "http://upload.liuzy88.com/DiskInfo.zip"
goto Step0
:Step5
echo 准备下载 ExeinfoPe.zip ...
wget -c -O "D:\soft\ExeinfoPe.zip" "http://upload.liuzy88.com/ExeinfoPe.zip"
goto Step0
:Step6
echo 准备下载 FastCopy.zip ...
wget -c -O "D:\soft\FastCopy.zip" "http://upload.liuzy88.com/FastCopy.zip"
goto Step0
:Step7
echo 准备下载 freeSSHd.zip ...
wget -c -O "D:\soft\freeSSHd.zip" "http://upload.liuzy88.com/freeSSHd.zip"
goto Step0
:Step8
echo 准备下载 GPUZ.zip ...
wget -c -O "D:\soft\GPUZ.zip" "http://upload.liuzy88.com/GPUZ.zip"
goto Step0
:Step9
echo 准备下载 HexCmp.zip ...
wget -c -O "D:\soft\HexCmp.zip" "http://upload.liuzy88.com/HexCmp.zip"
goto Step0
:Step10
echo 准备下载 ILSpy.zip ...
wget -c -O "D:\soft\ILSpy.zip" "http://upload.liuzy88.com/ILSpy.zip"
goto Step0
:Step11
echo 准备下载 MusicTools.zip ...
wget -c -O "D:\soft\MusicTools.zip" "http://upload.liuzy88.com/MusicTools.zip"
goto Step0
:Step12
echo 准备下载 Ollydbg.zip ...
wget -c -O "D:\soft\Ollydbg.zip" "http://upload.liuzy88.com/Ollydbg.zip"
goto Step0
:Step13
echo 准备下载 ProcView.exe ...
wget -c -O "D:\soft\ProcView.exe" "http://upload.liuzy88.com/ProcView.exe"
goto Step0
:Step14
echo 准备下载 Shadowsocks.zip ...
wget -c -O "D:\soft\Shadowsocks.zip" "http://upload.liuzy88.com/Shadowsocks.zip"
goto Step0
:Step15
echo 准备下载 SPY++.zip ...
wget -c -O "D:\soft\SPY++.zip" "http://upload.liuzy88.com/SPY++.zip"
goto Step0
:Step16
echo 准备下载 TcpView.exe ...
wget -c -O "D:\soft\TcpView.exe" "http://upload.liuzy88.com/TcpView.exe"
goto Step0
:Step17
echo 准备下载 WinNTSetup.zip ...
wget -c -O "D:\soft\WinNTSetup.zip" "http://upload.liuzy88.com/WinNTSetup.zip"
goto Step0
:Step18
echo 准备下载 一键修改系统信息.exe ...
wget -c -O "D:\soft\一键修改系统信息.exe" "http://upload.liuzy88.com/一键修改系统信息.exe"
goto Step0
:Step19
echo 准备下载 ant_1.3.4.zip ...
wget -c -O "D:\soft\ant_1.3.4.zip" "http://upload.liuzy88.com/ant_1.3.4.zip"
goto Step0
:Step20
echo 准备下载 ASPack.zip ...
wget -c -O "D:\soft\ASPack.zip" "http://upload.liuzy88.com/ASPack.zip"
goto Step0
:Step21
echo 准备下载 ConfuserEx.zip ...
wget -c -O "D:\soft\ConfuserEx.zip" "http://upload.liuzy88.com/ConfuserEx.zip"
goto Step0
:Step22
echo 准备下载 ConfuserExUn.zip ...
wget -c -O "D:\soft\ConfuserExUn.zip" "http://upload.liuzy88.com/ConfuserExUn.zip"
goto Step0
:Step23
echo 准备下载 de4dot.zip ...
wget -c -O "D:\soft\de4dot.zip" "http://upload.liuzy88.com/de4dot.zip"
goto Step0
:Step24
echo 准备下载 dotNET_Reactor.zip ...
wget -c -O "D:\soft\dotNET_Reactor.zip" "http://upload.liuzy88.com/dotNET_Reactor.zip"
goto Step0
:Step25
echo 准备下载 DSignTool.zip ...
wget -c -O "D:\soft\DSignTool.zip" "http://upload.liuzy88.com/DSignTool.zip"
goto Step0
:Step26
echo 准备下载 EasyWeb.zip ...
wget -c -O "D:\soft\EasyWeb.zip" "http://upload.liuzy88.com/EasyWeb.zip"
goto Step0
:Step27
echo 准备下载 freeSSHd.exe ...
wget -c -O "D:\soft\freeSSHd.exe" "http://upload.liuzy88.com/freeSSHd.exe"
goto Step0
:Step28
echo 准备下载 upx.exe ...
wget -c -O "D:\soft\upx.exe" "http://upload.liuzy88.com/upx.exe"
goto Step0
:Step29
echo 准备下载 VMProtect.zip ...
wget -c -O "D:\soft\VMProtect.zip" "http://upload.liuzy88.com/VMProtect.zip"
goto Step0
:Step30
echo 准备下载 先驱正式版2020081801.zip ...
wget -c -O "D:\soft\先驱正式版2020081801.zip" "http://upload.liuzy88.com/先驱正式版2020081801.zip"
goto Step0
:Step31
echo 准备下载 易语言5.9.zip ...
wget -c -O "D:\soft\易语言5.9.zip" "http://upload.liuzy88.com/易语言5.9.zip"
goto Step0
:Step32
echo 准备下载 神泣0777.zip ...
wget -c -O "D:\soft\神泣0777.zip" "http://upload.liuzy88.com/神泣0777.zip"
goto Step0
:Step33
echo 准备下载 神泣GM助手.zip ...
wget -c -O "D:\soft\神泣GM助手.zip" "http://upload.liuzy88.com/神泣GM助手.zip"
goto Step0
:Step34
echo 准备下载 神泣全能助手.zip ...
wget -c -O "D:\soft\神泣全能助手.zip" "http://upload.liuzy88.com/神泣全能助手.zip"
goto Step0
:Step35
echo 准备下载 神泣工具全集2011.zip ...
wget -c -O "D:\soft\神泣工具全集2011.zip" "http://upload.liuzy88.com/神泣工具全集2011.zip"
goto Step0
:Step36
echo 准备下载 鼎盛神泣合区工具破解版.zip ...
wget -c -O "D:\soft\鼎盛神泣合区工具破解版.zip" "http://upload.liuzy88.com/鼎盛神泣合区工具破解版.zip"
goto Step0
:Step37
echo 准备下载 ATI2021_v25.7.1.39184_x64_WinPE.exe ...
wget -c -O "D:\soft\ATI2021_v25.7.1.39184_x64_WinPE.exe" "http://upload.liuzy88.com/ATI2021_v25.7.1.39184_x64_WinPE.exe"
goto Step0
:Step38
echo 准备下载 ATI2021_v25.7.1.39184_x86_WinPE.exe ...
wget -c -O "D:\soft\ATI2021_v25.7.1.39184_x86_WinPE.exe" "http://upload.liuzy88.com/ATI2021_v25.7.1.39184_x86_WinPE.exe"
goto Step0
:Step39
echo 准备下载 DirectX_Repair_V3.9.7z ...
wget -c -O "D:\soft\DirectX_Repair_V3.9.7z" "http://upload.liuzy88.com/DirectX_Repair_V3.9.7z"
goto Step0
:Step40
echo 准备下载 Xshell.zip ...
wget -c -O "D:\soft\Xshell.zip" "http://upload.liuzy88.com/Xshell.zip"
goto Step0
pause